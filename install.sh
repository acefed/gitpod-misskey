#!/bin/bash

mkdir -p /workspace/ffmpeg
wget -P /workspace https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz
tar -xJf /workspace/ffmpeg-release-amd64-static.tar.xz --strip-components=1 -C /workspace/ffmpeg
rm /workspace/ffmpeg-release-amd64-static.tar.xz
